#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# @Time    : 2022/1/13 21:02
# @Author  : Hubert Shelley
# @Project  : starlette_utils
# @FileName: test.py.py
# @Software: PyCharm
"""
from starlette_utils.utils.decorators import thread_decorator

import time


@thread_decorator
def out_test(t: int):
    """
    out_test

    :param t:
    :return:
    """
    time.sleep(t)
    print(t)


if __name__ == '__main__':
    for _ in range(10, 1, -1):
        out_test(_)

    time.sleep(12)
