# Starlette_utils

starlette工具包

![avatar](https://cdn.smart7.tech/static/img/Perfect7.png)

## 项目依赖

|  包名   | 版本  |
|  ----  | ----  |
| [marshmallow] |  ≥ 3.13.0 |
| [starlette] |  ≥ 0.17.1 |
| [starlette-apispec] |  ≥ 1.0.4 |
| [smart7-orm] | 1.0.4 |
| [pyjwt] | ≥ 2.1.0 |

## 更新日志

> 0.0.15
> > 1. 修复BUG：打包成二进制包后异常无法正常抛出
> > 2. 项目初始化调整

<details>
<summary><mark>更多</mark></summary>

> 0.0.14
> > 1. 添加临时文件处理工具，程序打包后运行时可以用来清理之前因异常退出导致的临时文件无法清除问题
>
> 0.0.13
> > 1. 移除uvloop，处理windows无法安装
> 
> 0.0.12
> > 1. 添加命令行工具，快速创建项目
>
> 0.0.11
> > 1. Json返回格式补充
>
> 0.0.10
> > 1. 序列化器异常调整
> > 2. 视图过滤器异常待处理
>
> 0.0.9
> > 1. 异常修复
>
> 0.0.8
> > 1. 自身逻辑删除数据异常修复
>
> 0.0.6
> > 1. 修复put/patch更新接口出现的异常
>
> 0.0.5
> > 1. 序列化器序列化调整为异步
>
> 0.0.4
> > 1. 新增token刷新功能
>
> 0.0.3
> > 1. 修复update视图不能正常修改数据
> > 2. 新增schema动态配置
>
> 0.0.2
> > 新增jwt工具用于生成token
</details>

[marshmallow]: https://marshmallow.readthedocs.io/

[starlette]: https://www.starlette.io

[starlette-apispec]: https://apispec.readthedocs.io/en/stable/

[smart7-orm]: https://gitee.com/hubertshelley/smart7-orm

[pyjwt]: https://pyjwt.readthedocs.io/en/latest/
