#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# @Time    : 2021/12/21 1:47 下午
# @Author  : Hubert Shelley
# @Project  : starlette_utils
# @FileName: version.py
# @Software: PyCharm
"""
VERSION = '0.0.15'
